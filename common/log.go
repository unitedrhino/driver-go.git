package common

import (
	"context"
	"github.com/spf13/cast"
)

const (
	dbCtxDebugKey = "db.debug.type"
)

func NeedDebug(ctx context.Context) bool {
	if val := ctx.Value(dbCtxDebugKey); val != nil && cast.ToBool(val) == false { //不打印日志
		return false
	}
	return true
}
